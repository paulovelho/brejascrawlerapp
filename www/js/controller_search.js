
brewjasControllers.controller("searchController", 
	["$scope", "searchService",
	function($scope, searchService){
		$scope.query = searchService.query;
		$scope.result = searchService.result;

		$scope.isLoading = false;

		$scope.doSearch = function(){
			$scope.isLoading = true;
			searchService.doSearch($scope.query, function(data){
				$scope.result = data;
				$scope.isLoading = false;
			});
		};
		$scope.loadBeer = function(link){
			searchService.loadBeer(link);
			window.location.href="#/details"
		};
	}]
);
