brewjasServices.factory("brejasCrawlerConn", ["$http", 
	function($http){
		var urlConn = "http://brejascraw.herokuapp.com/";

		var searchBreja = function(query, callback){
			var url = urlConn + "search/" + query;
			$http.get(url)
				.then(function(response, data){
					callback(response);
				});
		};
		var getDetails = function(link, callback){
			var url = urlConn + "info" + link;
			$http.get(url)
				.then(function(response, data){
					callback(response);
				});
		};

		return {
			search: searchBreja,
			getBeerDetails: getDetails
		};
	}
]);

brewjasServices.factory("searchService",
	["brejasCrawlerConn", 
	function(brejasCrawlerConn){
		return {
			query: "",
			beerLink: "",
			result: null,
			doSearch: function(query, callback){
				this.query = query;
				brejasCrawlerConn.search(query, function(data){
					this.result = data.data;
					callback(this.result);
				}.bind(this));
			},
			loadBeer: function(link){
				this.beerLink = link;
			},
			getBeerDetails: function(callback){
				brejasCrawlerConn.getBeerDetails(this.beerLink, function(data){
					callback(data.data);
				});
			}
		};
	}]
);
