
brewjasModule = angular.module("brewjas", ["ionic", "brewjas.services", "brewjas.controllers"]);
brewjasServices = angular.module("brewjas.services", []);
brewjasControllers = angular.module("brewjas.controllers", ["brewjas.services", "ionic"]);


brewjasModule.config(function($stateProvider, $urlRouterProvider){
  $stateProvider
  .state("search", {
    url: "/search",
    templateUrl: "templates/search.html"
  })
  .state("details", {
    url: "/details",
    templateUrl: "templates/beerDetails.html"
  })
  ;

  $urlRouterProvider.otherwise("/search");

});
