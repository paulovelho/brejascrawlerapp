brewjasControllers.controller("beerDetailsController", 
	["$scope", "$ionicNavBarDelegate", "searchService",
	function($scope, $ionicNavBarDelegate, searchService){
		$scope.isLoading = true;
		$scope.beer;
		$scope.loadBreja = function(){
			$scope.isLoading = true;
			searchService.getBeerDetails(function(data){
				$scope.beer = data;
				$ionicNavBarDelegate.setTitle($scope.beer.name);
				$scope.isLoading = false;
			});
		};
		$scope.loadBreja();
	}]
);
